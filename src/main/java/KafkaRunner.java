import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class KafkaRunner {

    static ObjectMapper objectMapper = new ObjectMapper();


    public static void main(String[] args) {
        objectMapper.findAndRegisterModules();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:29092");
        props.put("linger.ms", 1);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<>(props);
        for (int i = 0; i < 5; i++) {
            WordEvent we = new WordEvent();
            try {
                producer.send(new ProducerRecord<String, String>("test", we.getKey(), objectMapper.writeValueAsString(we)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
            producer.close();
    }


}
