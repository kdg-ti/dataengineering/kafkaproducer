import java.time.LocalDateTime;
import java.util.Date;
import java.util.Random;

public class WordEvent {
    private LocalDateTime timestamp;
    private String key;
    private int value;
    private static Random random = new Random();
    private static String[] words=new String[] {"Monkey", "Fish", "Dog"};

    public WordEvent() {
        timestamp =  LocalDateTime.now().plusMinutes(-random.nextInt(5));
        this.key = words[random.nextInt(3)];
        this.value = 1;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
